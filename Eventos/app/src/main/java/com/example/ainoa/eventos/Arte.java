package com.example.ainoa.eventos;

/**
 * Created by Ainoa on 17/11/2015.
 */
public class Arte {

    private String coordenadas;
    private String titulo;
    private String link;

    public Arte(String coor, String titulo, String link){
        this.coordenadas = coor;
        this.titulo = titulo;
        this.link = link;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
