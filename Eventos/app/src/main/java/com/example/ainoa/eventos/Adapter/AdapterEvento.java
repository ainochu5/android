package com.example.ainoa.eventos.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ainoa.eventos.Evento;
import com.example.ainoa.eventos.R;

import java.util.ArrayList;

/**
 * Created by Ainoa on 06/11/2015.
 */
public class AdapterEvento extends BaseAdapter {

    private Context context;
    private ArrayList<Evento> listaEventos;
    private LayoutInflater inflater;

    public AdapterEvento(Context context, ArrayList<Evento> listaEventos){
        this.context = context;
        this.listaEventos = listaEventos;
        inflater = LayoutInflater.from(context);
    }

    static class ViewHolder {
        ImageView foto;
        TextView nombreEvento;
        TextView descripcion;
        TextView precio;
    }
    //QUE QUIERO HACER CON CADA AMIGO. COMO PINTARLO EN EL LAYOUT
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        // Si la View es null se crea de nuevo
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.formatoevento, null);

            holder = new ViewHolder();
            holder.foto = (ImageView) convertView.findViewById(R.id.imEvento);
            holder.nombreEvento = (TextView) convertView.findViewById(R.id.textNombre);
            holder.descripcion = (TextView) convertView.findViewById(R.id.textDescripcion);
            holder.precio = (TextView) convertView.findViewById(R.id.textPrecio);

            convertView.setTag(holder);
        }
		/*
		 *  En caso de que la View no sea null se reutilizar� con los
		 *  nuevos valores
		 */
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Evento evento = listaEventos.get(position);
        holder.foto.setImageBitmap(evento.getImagenE());
        holder.nombreEvento.setText(evento.getNombreE());
        holder.descripcion.setText(evento.getDescripcionE());
        holder.precio.setText(Float.toString(evento.getPrecioE()));

        return convertView;
    }

    //CUANTOS AMIGOS TENGO
    @Override
    public int getCount() {
        return listaEventos.size();
    }

    //CONSEGUIR AL AMIGO SEGUN LA LISTA
    @Override
    public Object getItem(int position) {
        return listaEventos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
