package com.example.ainoa.eventos;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.ainoa.eventos.Adapter.AdapterEvento;

import java.util.ArrayList;
import java.util.Date;

public class AgregarEvento extends Activity implements View.OnClickListener {

    public static ArrayList<Evento>LISTAEVENTOS = new ArrayList<Evento>();
    private static final int RESULTADO_CARGA_IMAGEN=5;
    private AdapterEvento adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento);

        //LLAMAR A LOS BOTONES Y CAJAS
        ImageButton btfoto = (ImageButton) findViewById(R.id.ibFoto);
        btfoto.setOnClickListener(this);
        Button btAceptar = (Button) findViewById(R.id.btAceptar);
        btAceptar.setOnClickListener(this);
        Button btCancelar = (Button) findViewById(R.id.btCancelar);
        btCancelar.setOnClickListener(this);

        EditText textNombreE = (EditText) findViewById(R.id.textNombreE);
        EditText textDescripcionE = (EditText) findViewById(R.id.textDescripcionE);
        EditText textDireccionE = (EditText) findViewById(R.id.textDireccionE);
        EditText textFechaE = (EditText) findViewById(R.id.textFechaE);
        EditText textPrecioE = (EditText) findViewById(R.id.textHoraE);
        EditText textHoraE = (EditText) findViewById(R.id.textPrecioE);
        EditText textAforoE = (EditText) findViewById(R.id.textAforoE);
        EditText textURL = (EditText) findViewById(R.id.textURL);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //CUANDO EL USUARIO SALGA DE LA GALERIA PILLE LA FOTO
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if((requestCode == RESULTADO_CARGA_IMAGEN)&& (resultCode ==RESULT_OK)&& (data !=null)){

            //Obtiene el Uri de la imagen seleccionada por el usuario
            Uri imagenSeleccionada = data.getData();
            String[] ruta = {MediaStore.Images.Media.DATA};

            //Realiza una consulta a la galaria de iamagenes solicitando la imagen

            Cursor cursor = getContentResolver().query(imagenSeleccionada, ruta, null, null, null);
            cursor.moveToFirst();


            //Obtiene la ruta de la imagen

            int indice = cursor.getColumnIndex(ruta[0]);
            String picturePath = cursor.getString(indice);
            cursor.close();

            //Carga la imagen en el botón ImageButton
            ImageButton btfoto = (ImageButton) findViewById(R.id.ibFoto);
            btfoto.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAceptar:
                Evento e = new Evento();
                EditText textNombreE = (EditText) findViewById(R.id.textNombreE);
                EditText textDescripcionE = (EditText) findViewById(R.id.textDescripcionE);
                EditText textDireccionE = (EditText) findViewById(R.id.textDireccionE);
                EditText textFechaE = (EditText) findViewById(R.id.textFechaE);
                EditText textPrecioE = (EditText) findViewById(R.id.textHoraE);
                EditText textHoraE = (EditText) findViewById(R.id.textPrecioE);
                EditText textAforoE = (EditText) findViewById(R.id.textAforoE);
                EditText textURL = (EditText) findViewById(R.id.textURL);
                ImageButton btfoto = (ImageButton) findViewById(R.id.ibFoto);
                e.setNombreE(textNombreE.getText().toString());
                e.setDescripcionE(textDescripcionE.getText().toString());
                e.setDireccionE(textDireccionE.getText().toString());
                //e.setFechaE((Date) textFechaE.getText());
                e.setPrecioE((0));
                e.setAforoE(Integer.parseInt(textAforoE.getText().toString()));
                e.setUrlE(textURL.getText().toString());
                e.setImagenE(((BitmapDrawable) (btfoto.getDrawable())).getBitmap());
                LISTAEVENTOS.add(e);
                break;
            case R.id.btCancelar:
                Intent intent = new Intent(this, Main.class);
                startActivity(intent);
                break;
            case R.id.ibFoto:
                Intent intentfoto = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(intentfoto, RESULTADO_CARGA_IMAGEN);
            default:
        }
    }
}
