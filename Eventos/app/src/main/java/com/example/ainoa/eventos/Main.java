package com.example.ainoa.eventos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ListView;
import static com.example.ainoa.eventos.CargarJSON.LISTAARTE;
import com.example.ainoa.eventos.Adapter.AdapterArte;
import com.example.ainoa.eventos.Adapter.AdapterEvento;

public class Main extends Activity {

    private AdapterArte adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        CargarJSON Json = new CargarJSON();
        //OBTENER EL LISTVIEW

        ListView lista = (ListView) findViewById(R.id.ltEventos);
        adapter = new AdapterArte(this, R.layout.formatoarte,LISTAARTE);
        lista.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.botonanadir, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_agregarevento) {
            Intent intent = new Intent(this, AgregarEvento.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
