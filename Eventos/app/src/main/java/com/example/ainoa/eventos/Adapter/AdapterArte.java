package com.example.ainoa.eventos.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ainoa.eventos.Arte;
import com.example.ainoa.eventos.Evento;
import com.example.ainoa.eventos.R;

import java.util.ArrayList;

/**
 * Created by Ainoa on 17/11/2015.
 */
public class AdapterArte extends ArrayAdapter<Arte> {

    private Context context;
    private ArrayList<Arte> listaArte;
    private int layoutId;

    public AdapterArte(Context context,int layoutId,ArrayList<Arte> listaArte){
        super(context, layoutId, listaArte);
        this.context = context;
        this.listaArte = listaArte;
        this.layoutId = layoutId;
        LayoutInflater inflater = LayoutInflater.from(context);
    }


    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        View fila = view;
        ItemArte item = null;

        if (fila == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            fila = inflater.inflate(layoutId, padre, false);

            item = new ItemArte();
            item.titulo = (TextView) fila.findViewById(R.id.textNombree);
            item.coordenadas = (TextView) fila.findViewById(R.id.textCoordenadas);
            item.link = (TextView) fila.findViewById(R.id.textLink);

            fila.setTag(item);
        } else {
            item = (ItemArte) fila.getTag();
        }

        Arte arte = listaArte.get(posicion);
        item.titulo.setText(arte.getTitulo());
        item.coordenadas.setText(arte.getLink());
        item.link.setText(arte.getCoordenadas());

        return fila;
    }
    static class ItemArte {

        TextView titulo;
        TextView coordenadas;
        TextView link;
    }
}
