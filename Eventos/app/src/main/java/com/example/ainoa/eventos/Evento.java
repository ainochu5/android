package com.example.ainoa.eventos;

import android.graphics.Bitmap;
import android.widget.ImageButton;

import java.util.Date;

/**
 * Created by Ainoa on 05/11/2015.
 */
public class Evento {

    private String nombreE;
    private String descripcionE;
    private String direccionE;
    private Date fechaE;
    private String horaE;
    private float precioE;
    private int aforoE;
    private Bitmap imagenE;
    private String urlE;

    public String getNombreE() {
        return nombreE;
    }

    public void setNombreE(String nombreE) {
        this.nombreE = nombreE;
    }

    public String getDescripcionE() {
        return descripcionE;
    }

    public void setDescripcionE(String descripcionE) {
        this.descripcionE = descripcionE;
    }

    public String getDireccionE() {
        return direccionE;
    }

    public void setDireccionE(String direccionE) {
        this.direccionE = direccionE;
    }

    public Date getFechaE() {
        return fechaE;
    }

    public void setFechaE(Date fechaE) {
        this.fechaE = fechaE;
    }

    public String getHoraE() {
        return horaE;
    }

    public void setHoraE(String horaE) {
        this.horaE = horaE;
    }

    public float getPrecioE() {
        return precioE;
    }

    public void setPrecioE(float precioE) {
        this.precioE = precioE;
    }

    public int getAforoE() {
        return aforoE;
    }

    public void setAforoE(int aforoE) {
        this.aforoE = aforoE;
    }

    public Bitmap getImagenE() {
        return imagenE;
    }

    public void setImagenE(Bitmap imagenE) {
        this.imagenE = imagenE;
    }

    public String getUrlE() {
        return urlE;
    }

    public void setUrlE(String urlE) {
        this.urlE = urlE;
    }
}


